(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))

;; Dashboard
(use-package dashboard
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-startup-banner "~/.config/emacs/emacs-icon-254x256-iitz9a09.png")
  (setq dashboard-banner-logo-title "L coder :)"))
(setq inhibit-startup-screen t)

;; Dashboard emacsclient
(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
(defun my/disable-scroll-bars (frame)
  (modify-frame-parameters frame
                           '((vertical-scroll-bars . nil)
                             (horizontal-scroll-bars . nil))))
(add-hook 'after-make-frame-functions 'my/disable-scroll-bars)

(setq dashboard-display-icons-p t)

;; Themes 
(use-package modus-themes
  :ensure t
  :config
  (modus-themes-load-theme 'modus-operandi)
  (define-key global-map (kbd "<f5>") #'modus-themes-toggle))

;; Auctex LaTex
(use-package auctex
  :commands LaTeX-mode
  :hook (LaTeX-mode . (lambda ()
			(push (list 'output-pdf "Zathura")
			      TeX-view-program-selection))))
;; completion
(use-package company
  :ensure t
  :defer t
  :config
  (add-hook 'after-init-hook 'global-company-mode))

;; ui things
(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)

;; font
(set-face-attribute 'default nil :font "Source Code Pro-14:weight=Regular")
(setq default-frame-alist '((font . "Source Code Pro-14")))

;; Backups
(setq backup-directory-alist '(("." . "~/.emacs_saves")))

;; Smex
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;; ido
(use-package ido-vertical-mode
  :ensure t
  :config
  (setq ido-enable-flex-matching t)
  (setq ido-everywhere t)
  (setq ido-vertical-define-keys 'C-n-and-C-p-only)      
  :init
  (ido-mode 1)
  (ido-vertical-mode 1))

;; Relative numbers
(setq display-line-numbers-type 'relative)
(global-display-line-numbers-mode)

;; vim ci and co
(use-package expand-region
  :bind ("C-=" . er/expand-region))

(use-package change-inner
  :bind ("M-i" . change-inner)
  ("M-o" . change-outer))

(use-package org-bullets
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(use-package eglot
  :custom
  (eldoc-echo-area-use-multiline-p 3)
  (eldoc-echo-area-display-truncation-message nil)
  )

(defun split-and-follow-horizontally ()
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)

(defun split-and-follow-vertically ()
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1))
(global-set-key (kbd "C-x 3") 'split-and-follow-vertically)
